import { Formik } from 'formik';
import React from 'react';
import { Button, Form, Header, Segment } from 'semantic-ui-react';
import MyTextArea from '../../app/common/form/MyTextArea';
import MyTextInput from '../../app/common/form/MyTextInput';
import { Profile } from '../../app/models/profile';
import * as Yup from 'yup';

interface Props {
    profile: Profile;
    formSubmit: (profile: Partial<Profile>) => void;
}

export default function ProfileEditForm({profile, formSubmit}: Props) {

    const validationSchema = Yup.object({
        displayName: Yup.string().required('The display name is required'),
    });

    return (
        <Formik 
            enableReinitialize 
            initialValues={profile} 
            onSubmit={values => formSubmit(values)}
            validationSchema={validationSchema}
        >
            {({ handleSubmit, isValid, isSubmitting, dirty }) => (
                <Form className='ui form' onSubmit={handleSubmit} autoComplete='off' >
                    <MyTextInput name='displayName' placeholder='Display Name' />
                    <MyTextArea rows={3} placeholder='Add your bio' name='bio' />
                    <Button 
                        disabled={isSubmitting || !dirty || !isValid}
                        loading={isSubmitting} 
                        floated='right' 
                        positive 
                        type='submit' 
                        content='Update profile' 
                    />
                </Form>
            )}
        </Formik>
    )
}