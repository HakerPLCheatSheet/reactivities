using System.Linq;
using Application.Activities;
using AutoMapper;
using Domain;

namespace Application.Core
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Activity, Activity>();
            CreateMap<Activity, ActivityDto>()
                .ForMember(destiny => destiny.HostUsername, options => options.MapFrom(source =>
                            source.Attendees.FirstOrDefault(x => x.IsHost).AppUser.UserName));

            CreateMap<ActivityAttendee, AttendeeDto>()
                .ForMember(destiny => destiny.DisplayName, options => options.MapFrom(s => s.AppUser.DisplayName))
                .ForMember(destiny => destiny.Username, options => options.MapFrom(s => s.AppUser.UserName))
                .ForMember(destiny => destiny.Bio, options => options.MapFrom(s => s.AppUser.Bio))
                .ForMember(destiny => destiny.Image, options =>
                        options.MapFrom(source => source.AppUser.Photos.FirstOrDefault(x => x.IsMain).Url));

            CreateMap<AppUser, Profiles.Profile>()
                .ForMember(destiny => destiny.Image, options =>
                        options.MapFrom(source => source.Photos.FirstOrDefault(x => x.IsMain).Url));
        }
    }
}