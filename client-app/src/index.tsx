import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import 'react-calendar/dist/Calendar.css';
import 'react-toastify/dist/ReactToastify.min.css';
import 'react-datepicker/dist/react-datepicker.css';
import './app/layout/styles.css';
import App from './app/layout/App';
import reportWebVitals from './reportWebVitals';
import { store, StoreContext } from './app/stores/store';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';


export const history = createBrowserHistory();


ReactDOM.render(
  //<React.StrictMode>
  <StoreContext.Provider value={store}>
    <Router history={history}>   {/*    BrowserRouter -   Router pracuje na niższym poziomie */}
      <App />
    </Router>
  </StoreContext.Provider>,
  //</React.StrictMode>, remove strict because we may have problems with 3th part dll that are not update to react 17
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
